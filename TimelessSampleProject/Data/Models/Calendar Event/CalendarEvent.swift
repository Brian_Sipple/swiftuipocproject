//
//  CalendarEvent.swift
//  TimelessSampleProject
//
//  Created by CypherPoet on 3/24/20.
// ✌️
//

import Foundation
import CoreLocation


struct CalendarEvent {
    let id = UUID()
    
    var title: String
    var longDescription: String
    
    var startDate: Date
    var endDate: Date
    
    var cardImageName: String
//    var dateInterval: DateInterval
    
    var repeatsWeekly: Bool
    var repeatsWeeklyDescription: String?
    var dateOfLastRepeat: Date?
    
    var attendanceStatus: AttendanceStatus = .pendingResponse
    var location: EventLocation?
}

extension CalendarEvent: Identifiable {}
extension CalendarEvent: Hashable {}


// MARK: - Computeds
extension CalendarEvent {
    var isUpcoming: Bool { startDate.timeIntervalSinceNow > 0 }

    
    var minutesToStart: Int {
        let diffComponents = Calendar.current.dateComponents(
            [.minute],
            from: Date(),
            to: startDate
        )
        
        return diffComponents.minute ?? 0
    }
}

