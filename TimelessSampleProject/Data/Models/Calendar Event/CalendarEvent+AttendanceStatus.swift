//
//  CalendarEvent+AttendanceStatus.swift
//  TimelessSampleProject
//
//  Created by Brian Sipple on 3/25/20.
//  Copyright © 2020 CypherPoet. All rights reserved.
//

import Foundation


extension CalendarEvent {
    enum AttendanceStatus {
        case going
        case notGoing
        case maybeGoing
        case pendingResponse
    }
}
