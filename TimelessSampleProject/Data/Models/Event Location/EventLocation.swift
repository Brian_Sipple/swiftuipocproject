//
//  EventLocation.swift
//  TimelessSampleProject
//
//  Created by Brian Sipple on 3/25/20.
//  Copyright © 2020 CypherPoet. All rights reserved.
//

import Foundation
import CoreLocation


struct EventLocation {
    let id = UUID()
    
    var latitude: CLLocationDegrees
    var longitude: CLLocationDegrees
    
    var title: String
    
    // 📝 These can be created on device using geo-coded CLPlacemark objects (https://developer.apple.com/documentation/corelocation/clplacemark),
    // but here I'm assuming that these values are generated on the backend.
    var addressLine: String
}



extension EventLocation: Identifiable {}
extension EventLocation: Hashable {}
