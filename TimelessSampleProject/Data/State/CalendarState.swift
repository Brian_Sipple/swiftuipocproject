//
//  CalendarState.swift
//  TimelessSampleProject
//
//  Created by CypherPoet on 3/25/20.
// ✌️
//


import Foundation
import Combine
import CypherPoetSwiftUIKit_DataFlowUtils


struct CalendarState {
    var upcomingEvents: [CalendarEvent] = []
    var actionModalEvent: CalendarEvent?
}



enum CalendarSideEffect: SideEffect {
    case fetchUpcomingEvents

    
    func mapToAction() -> AnyPublisher<AppAction, Never> {
        switch self {
        case .fetchUpcomingEvents:
            return CurrentApp.localDBService
                .fetchUpcomingEvents()
                
                // 📝 This would be a good spot to implement some more robust error
                // handling -- perhaps using an `enum` to define the current fetching state.
                .replaceError(with: [])
                
                .map { AppAction.calendar(.upcomingEventsSet($0)) }
                .eraseToAnyPublisher()
        }
    }
}



enum CalendarAction {
    case actionModalEventSet(_ event: CalendarEvent?)
    case upcomingEventsSet(_ events: [CalendarEvent])
}


// MARK: - Reducer
let calendarReducer: Reducer<CalendarState, CalendarAction> = Reducer(
    reduce: { state, action in
        switch action {
        case .upcomingEventsSet(let events):
            state.upcomingEvents = events
        case .actionModalEventSet(let event):
            state.actionModalEvent = event
        }
    }
)

