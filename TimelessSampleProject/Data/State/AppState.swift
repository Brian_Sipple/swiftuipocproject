//
//  AppState.swift
//  TimelessSampleProject
//
//  Created by CypherPoet on 3/25/20.
// ✌️
//


import Foundation
import Combine
import CypherPoetSwiftUIKit_DataFlowUtils


struct AppState {
    var calendarState = CalendarState()
}



//enum AppSideEffect: SideEffect {
//
//}



enum AppAction {
    case calendar(_ action: CalendarAction)
}


// MARK: - Reducer
let appReducer: Reducer<AppState, AppAction> = Reducer(
    reduce: { appState, appAction in
        switch appAction {
        case .calendar(let action):
            calendarReducer.reduce(&appState.calendarState, action)
        }
    }
)
