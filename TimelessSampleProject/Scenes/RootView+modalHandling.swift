//
//  RootView+modalHandling.swift
//  TimelessSampleProject
//
//  Created by CypherPoet on 3/27/20.
// ✌️
//

import SwiftUI


// MARK: - Computeds
extension RootView {
    var isShowingEventModal: Bool {
        store.state.calendarState.actionModalEvent != nil
    }
    
    var modalShiftHeightThreshold: CGFloat { UIScreen.main.bounds.height * 0.25 }
}


// MARK: - Animations & Transitions
extension RootView {
    
    var actionModalAppearanceAnimation: Animation {
        Animation.easeOut(duration: Constants.Animation.Duration.modalAppearance)
    }
    
    var actionModalTransition: AnyTransition {
        AnyTransition
            .move(edge: .bottom)
            .animation(actionModalAppearanceAnimation)
    }
}


// MARK: - Gestures
extension RootView {
    
    /// Logic:
    ///     - Drag down beyond a set threshold to have the modal be dismissed fully or returned to the "peeking"
    ///     state -- depending on the force of the drag and whether or not it's fully expanded.
    ///     - Drag up beyond a set threshold to have the modal become fully expanded.
    var actionModalGesture: some Gesture {
        DragGesture()
            .updating($modalDragState, body: { (dragValue, dragState, _) in
                dragState = .dragging(translation: dragValue.translation)
            })
            .onEnded { finalDragValue in
                let verticalDragAmount = finalDragValue.predictedEndLocation.y - finalDragValue.startLocation.y
                
                if verticalDragAmount > 0 {
                    // ⏬ Downward Drag
                    self.handleDownwardDrag(amount: abs(verticalDragAmount))
                } else {
                    // ⏫ Upward Drag
                    if abs(verticalDragAmount) > self.modalShiftHeightThreshold {
                        self.fullyExpandModal()
                    }
                }
            }
    }
}


// MARK: - Private Helpers
private extension RootView {
    
    func handleDownwardDrag(amount dragAmount: CGFloat) {
        let fullCollapseHeightThreshold: CGFloat = modalShiftHeightThreshold * 2
        
        switch modalViewState {
        case .fullyExpanded:
            if dragAmount > fullCollapseHeightThreshold {
                collapseModal()
            } else if dragAmount > modalShiftHeightThreshold {
                modalViewState = .peeking
            }
        case .peeking:
            if dragAmount > modalShiftHeightThreshold {
                collapseModal()
            }
        }
    }
    
    
    func collapseModal() {
        modalViewState = .peeking
        store.send(.calendar(.actionModalEventSet(.none)))
    }
    
    func fullyExpandModal() {
        modalViewState = .fullyExpanded
    }
}


// MARK: - DragState
extension RootView {
    enum DragState {
        case inactive
        case dragging(translation: CGSize)
    }
}

extension RootView.DragState {
    
    var verticalDragDistance: CGFloat {
        switch self {
        case .inactive:
            return .zero
        case .dragging(let translation):
            return translation.height
        }
    }
}


// MARK: - ModalViewState
extension RootView {
    enum ModalViewState {
        case peeking
        case fullyExpanded
    }
}

extension RootView.ModalViewState {
    
    var anchorOffset: CGFloat {
        switch self {
        case .peeking:
            return UIScreen.main.bounds.height * 0.5
        case .fullyExpanded:
            return 16
        }
    }
}
