//
//  RootView.swift
//  TimelessSampleProject
//
//  Created by Brian Sipple on 3/24/20.
//  Copyright © 2020 CypherPoet. All rights reserved.
//

import SwiftUI

// 📝 I usually start with a "root" container so there's a well-defined
// place for setting up other potential user flows.
struct RootView {
    @EnvironmentObject var store: AppStore
    
    enum Tab {
        case inbox
        case home
        case createNew
    }
    
    @State var activeTab: Tab = .home
    
    @State var modalViewState: ModalViewState = .peeking
    @GestureState var modalDragState: DragState = .inactive
}


// MARK: - View
extension RootView: View {
    
    var body: some View {
        ZStack {
            TabView(selection: $activeTab) {
                Text("Inbox")
                    .tabItem {
                        Image(systemName: "envelope.fill")
                        Text("Inbox")
                }
                .tag(Tab.inbox)
                
                
                HomeContainerView()
                    .tabItem {
                        Image(systemName: "calendar.circle.fill")
                        Text("Calendar")
                }
                .tag(Tab.home)
                
                
                Text("New")
                    .tabItem {
                        Image(systemName: "calendar.badge.plus")
                        Text("New")
                }
                .tag(Tab.createNew)
            }
            .overlay(Color.black.opacity(isShowingEventModal ? 0.5 : 0.0))
            .animation(actionModalAppearanceAnimation)
            .allowsHitTesting(isShowingEventModal == false)

            
            if isShowingEventModal {
                CalendarEventActionModal(
                    event: store.state.calendarState.actionModalEvent!
                )
                .transition(actionModalTransition)
                .offset(
                    x: 0,
                    y: max(16, modalViewState.anchorOffset + modalDragState.verticalDragDistance)
                )
                .animation(.easeOut)
                .gesture(actionModalGesture)
                .zIndex(1)
            }
        }
    }
}




struct RootView_Previews: PreviewProvider {
    static var previews: some View {
        RootView()
            .environmentObject(PreviewData.AppStores.initial)
    }
}
