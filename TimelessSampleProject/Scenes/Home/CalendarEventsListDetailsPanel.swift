//
//  CalendarEventsListDetailsPanel.swift
//  TimelessSampleProject
//
//  Created by CypherPoet on 3/26/20.
// ✌️
//

import SwiftUI


struct CalendarEventsListDetailsPanel {
    var event: CalendarEvent
}


// MARK: - View
extension CalendarEventsListDetailsPanel: View {

    var body: some View {
        VStack {
            Text(event.longDescription)
            Spacer()
        }
        .padding()
        .background(Color.white)
    }
}


// MARK: - Preview
struct CalendarEventsListDetailsPanel_Previews: PreviewProvider {

    static var previews: some View {
        CalendarEventsListDetailsPanel(
            event: PreviewData.CalendarEvents.sample1
        )
    }
}
