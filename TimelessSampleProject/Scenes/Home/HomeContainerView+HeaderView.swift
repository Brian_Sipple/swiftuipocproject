//
//  HomeContainerView+HeaderView.swift
//  TimelessSampleProject
//
//  Created by CypherPoet on 3/27/20.
// ✌️
//

import SwiftUI


extension HomeContainerView {
    struct HeaderView {

    }
}


// MARK: - View
extension HomeContainerView.HeaderView: View {

    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            Text(Date.todaysDayText.uppercased())
                .font(.callout)
                .fontWeight(.semibold)
         
            HStack {
                Text("Today")
                    .font(.largeTitle)
                    .fontWeight(.bold)

                Spacer()
                
                HStack {
                    Image(systemName: "sun.min.fill")
                    Text("69°F")
                }
            }
        }
        .padding()
    }
}


// MARK: - Computeds
extension HomeContainerView.HeaderView {
}


// MARK: - View Variables
extension HomeContainerView.HeaderView {
}


// MARK: - Private Helpers
private extension HomeContainerView.HeaderView {
}



// MARK: - Preview
struct HomeContainerView_HeaderView_Previews: PreviewProvider {

    static var previews: some View {
        HomeContainerView.HeaderView()
    }
}
