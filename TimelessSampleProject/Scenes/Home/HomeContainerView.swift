//
//  HomeContainerView.swift
//  TimelessSampleProject
//
//  Created by CypherPoet on 3/24/20.
// ✌️
//

import SwiftUI


struct HomeContainerView {
    @EnvironmentObject private var store: AppStore
    @State private var isEventExpanded = false
}


// MARK: - View
extension HomeContainerView: View {

    var body: some View {
        VStack(spacing: 0) {
            HeaderView()
                .frame(height: headerViewHeight)
            
            CalendarEventsListView(
                viewModel: .init(calendarEvents: calendarState.upcomingEvents),
                isEventExpanded: $isEventExpanded,
                onActionMenuActivated: self.actionMenuActivated(for:),
                isActionMenuShowing: isActionMenuShowing
            )
            .offset(x: 0, y: eventsListYOffset)
        }
        .edgesIgnoringSafeArea(isEventExpanded ? [.top] : [])
        .animation(.easeOut(duration: Constants.Animation.Duration.cardSlide))
        .onAppear(perform: fetchCalendarEvents)
    }
}


// MARK: - Computeds
extension HomeContainerView {
    var calendarState: CalendarState { store.state.calendarState }
    
    var isActionMenuShowing: Bool {
        calendarState.actionModalEvent != nil
    }
    
    var headerViewHeight: CGFloat {
        isEventExpanded ? .zero : Constants.Appearance.homeHeaderHeight
    }
    
    var eventsListYOffset: CGFloat {
        Constants.Appearance.homeHeaderHeight * (isEventExpanded ? -1.5 : -0.5)
    }
}


// MARK: - Private Helpers
private extension HomeContainerView {

    func fetchCalendarEvents() {
        store.send(CalendarSideEffect.fetchUpcomingEvents)
    }
    
    
    func actionMenuActivated(for event: CalendarEvent) {
        store.send(.calendar(.actionModalEventSet(event)))
    }
}



// MARK: - Preview
struct HomeContainerView_Previews: PreviewProvider {

    static var previews: some View {
        TabView {
            HomeContainerView()
                .tabItem {
                    Image(systemName: "calendar.circle.fill")
                    Text("Calendar")
                }
        }
        .environmentObject(PreviewData.AppStores.initial)
    }
}
