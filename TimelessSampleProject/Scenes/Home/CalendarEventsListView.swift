//
//  CalendarEventsListView.swift
//  TimelessSampleProject
//
//  Created by CypherPoet on 3/25/20.
// ✌️
//

import SwiftUI
import CypherPoetSwiftUIKit


struct CalendarEventsListView {
    @ObservedObject var viewModel: ViewModel
    @Binding var isEventExpanded: Bool

    var onActionMenuActivated: ((CalendarEvent) -> Void)?
    var isActionMenuShowing: Bool = false
    
    /// The percentage of the stack frame height that the user will need to drag in order to register a
    /// slide up or slide down of a card in the list -- or a dismissal of an expanded card.
    private let heightPercentageForRegisteringDrag: CGFloat = 0.25
    
    @State private var focusedIndex: Int = 0
    @GestureState private var stackedCardSideSwipeState: SideSwipeState = .inactive
}


// MARK: - View
extension CalendarEventsListView: View {

    var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .top) {
                self.cardStack
                    
                if self.isEventExpanded {
                    CalendarEventsListDetailsPanel(event: self.focusedEvent)
                        .frame(maxHeight: .infinity)
                        .offset(x: 0, y: self.heightForExpandedCard)
                        .transition(self.detailsPanelTransition)
                }
            }
            .frame(width: geometry.size.width, height: geometry.size.height)
//            .gesture(self.cardStackGesture(in: geometry))
        }
        .padding(.horizontal, self.isEventExpanded ? 0 : 16)
        .edgesIgnoringSafeArea(.top)
    }
}


// MARK: - View Variables
extension CalendarEventsListView {
    
    private var cardStack: some View {
        GeometryReader { geometry in
            ZStack {
                ForEach(self.viewModel.calendarEvents.indexed(), id: \.1.self) { (index, event) in
                    CalendarEventsListCard(
                        viewModel: .init(
                            event: event,
                            displayMode: (self.isEventExpanded && index == self.focusedIndex) ?
                                .expansion
                                : .listFace
                        )
                    )
                    .frame(
                        width: geometry.size.width,
                        height: self.height(forCardAt: index, in: geometry)
                    )
                    .gesture(self.cardGesture(in: geometry))
                    .allowsHitTesting(self.isCardInteractive(at: index))
                    .opacity(self.isCardInteractive(at: index) ? 1.0 : 0.0)
//                    .offset(x: 0, y: self.offsetHeight(forCardAt: index, in: geometry))
                    .offset(
                        x: self.offsetFromSideSwipe(forCardAt: index, in: geometry),
                        y: self.offsetHeight(forCardAt: index, in: geometry)
                    )
                    .zIndex(self.zIndex(forCardAt: index))
                    .animation(self.cardSlideAnimation)
                }
            }
        }
    }
}


// MARK: - Computeds
extension CalendarEventsListView {
    var focusedEvent: CalendarEvent { viewModel.calendarEvents[focusedIndex] }
    
    var heightForExpandedCard: CGFloat {
        max(UIScreen.main.bounds.height * 0.667, Constants.Appearance.minCardHeight)
    }
}
    

// MARK: - Animations & Transitions
extension CalendarEventsListView {

    var cardSlideAnimation: Animation {
        Animation.easeOut(duration: Constants.Animation.Duration.cardSlide)
    }
    
    var detailsPanelTransition: AnyTransition {
        let insertionDelay = Constants.Animation.Duration.cardSlide
        
        return AnyTransition.asymmetric(
            insertion: AnyTransition
                .opacity
                .animation(cardSlideAnimation.delay(insertionDelay)),
            removal: AnyTransition
                .opacity
                .animation(cardSlideAnimation)
        )
    }
}


// MARK: - Gestures
extension CalendarEventsListView {
    
    /// Long Press to trigger action menu.  (Ignore if the card is expanded.)
    var stackLongPressGesture: some Gesture {
        LongPressGesture(minimumDuration: 0.4)
            .onEnded { longPressWasCompleted in
                if longPressWasCompleted && self.isEventExpanded == false {
                    self.onActionMenuActivated?(self.focusedEvent)
                }
            }
    }
    
    
    /// Tap to trigger expansion. (Ignore if the card is already expanded.)
    var stackTapGesture: some Gesture {
        TapGesture()
            .onEnded {
                if self.isEventExpanded == false {
                    self.isEventExpanded = true
                }
            }
    }
    
    
    /// Logic:
    ///     - Drag down to have the active card be "dismissed" back down "off" the stack.
    ///     - Drag up to have the next card be "raised" on to the top of the stack.
    ///
    /// From the Project Google Doc: "an up/down swipe moves whole cards."
    func stackedCardDragGesture(in geometry: GeometryProxy) -> AnyGesture<DragGesture.Value> {
        AnyGesture(
            DragGesture()
                .updating($stackedCardSideSwipeState, body: { (value, stackedCardSideSwipeState, _) in
                    let translation = value.translation
                    
                    if abs(translation.width) > geometry.size.width * 0.4 {
                        stackedCardSideSwipeState = .committedSwipe(translation: value.translation)
                    } else {
                        stackedCardSideSwipeState = .peekingSwipe(translation: value.translation)
                    }
                })
                .onEnded { finalDragValue in
                    switch finalDragValue.dominantDirection {
                    case .left:
                         self.completeSwipeToLeft()
                    case .right:
                        self.completeSwipeToRight()
                    case .up,
                         .down:
                        self.handleVerticalCardDrag(in: geometry, with: finalDragValue)
                    case .none:
                        break
                    }
                }
        )
    }
    
    
    func expandedCardDragGesture(in geometry: GeometryProxy) -> AnyGesture<DragGesture.Value> {
        AnyGesture(
            DragGesture()
                .onEnded { finalDragValue in
                    let heightThreshold = self.cardShiftHeightThreshold(in: geometry)
                    let verticalDragAmount = finalDragValue.predictedEndLocation.y - finalDragValue.startLocation.y

                    if verticalDragAmount > 0 {
                        // ⏬ Downward Drag
                        if abs(verticalDragAmount) > heightThreshold {
                            self.isEventExpanded = false
                        }
                    }
                }
        )
    }
    
    
    func cardGesture(in geometry: GeometryProxy) -> some Gesture {
        let dragGesture = isEventExpanded ? expandedCardDragGesture(in: geometry) : stackedCardDragGesture(in: geometry)
        
        return stackTapGesture
            .exclusively(before: stackLongPressGesture)
            .simultaneously(with: dragGesture)
    }
}


// MARK: - Shifting & Swiping Helpers
extension CalendarEventsListView {
    
    func shiftCardsUp() {
        focusedIndex = min(focusedIndex + 1, viewModel.cardCount - 1)
    }
    
    func shiftCardsDown() {
        focusedIndex = max(focusedIndex - 1, 0)
    }
    
    
    func completeSwipeToLeft() {
        
    }
    
    
    func completeSwipeToRight() {
    }
    
    
    func handleVerticalCardDrag(in geometry: GeometryProxy, with finalDragValue: DragGesture.Value) {
        let heightThreshold = cardShiftHeightThreshold(in: geometry)
        let verticalDragAmount = finalDragValue.predictedEndLocation.y - finalDragValue.startLocation.y
        
        if verticalDragAmount > 0 {
            // ⏬ Downward Drag
            if abs(verticalDragAmount) > heightThreshold {
                self.shiftCardsDown()
            }
        } else {
            // ⏫ Upward Drag
            if abs(verticalDragAmount) > heightThreshold {
                self.shiftCardsUp()
            }
        }
    }
}


// MARK: - Misc Private Helpers
private extension CalendarEventsListView {
    
    func isCardInteractive(at stackIndex: Int) -> Bool {
        stackIndex == focusedIndex || stackIndex == focusedIndex + 1
    }
    
    
    func height(forCardAt stackIndex: Int, in stackContainer: GeometryProxy) -> CGFloat {
        if stackIndex == focusedIndex && isEventExpanded {
            return heightForExpandedCard
        } else {
            return max(stackContainer.size.height * 0.75, Constants.Appearance.minCardHeight)
        }
    }
    
    
    func cardShiftHeightThreshold(in geometry: GeometryProxy) -> CGFloat {
        let frameHeight = geometry.frame(in: .local).size.height
        return frameHeight * heightPercentageForRegisteringDrag
    }
    
    
    func offsetHeight(forCardAt stackIndex: Int, in stackContainer: GeometryProxy) -> CGFloat {
        if
            stackIndex == focusedIndex && isEventExpanded ||
            stackIndex <= focusedIndex
        {
            return 0
        } else {
            let indicesAwayFromFocusedIndex = CGFloat(stackIndex - focusedIndex)
            let cardHeight = height(forCardAt: stackIndex , in: stackContainer)
            
            return (cardHeight * indicesAwayFromFocusedIndex) + 16
        }
    }
    
    
    func offsetFromSideSwipe(forCardAt stackIndex: Int, in stackContainer: GeometryProxy) -> CGFloat {
        if stackIndex != focusedIndex { return 0 }
        
        return stackedCardSideSwipeState.sideSwipeDistance
    }
    

    func zIndex(forCardAt stackIndex: Int) -> Double {
        if stackIndex <= focusedIndex + 1 {
            return 1.0
        } else {
            return Double(-stackIndex)
        }
    }
}


// MARK: - SideSwipeState
extension CalendarEventsListView {
    enum SideSwipeState {
        case inactive
        case peekingSwipe(translation: CGSize)
        case committedSwipe(translation: CGSize)
    }
}

extension CalendarEventsListView.SideSwipeState {
    
    var sideSwipeDistance: CGFloat {
        switch self {
        case .inactive:
            return .zero
        case .peekingSwipe(let translation),
             .committedSwipe(let translation):
            return translation.width
        }
    }
}




// MARK: - Preview
struct CalendarEventsListView_Previews: PreviewProvider {

    static var previews: some View {
        CalendarEventsListView(
            viewModel: .init(calendarEvents: PreviewData.CalendarEvents.all),
            isEventExpanded: .constant(false),
            isActionMenuShowing: false
        )
    }
}
