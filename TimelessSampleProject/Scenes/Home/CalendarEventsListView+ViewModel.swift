//
//  CalendarEventsListView+ViewModel.swift
//  TimelessSampleProject
//
//  Created by CypherPoet on 3/25/20.
// ✌️
//


import SwiftUI
import Combine


extension CalendarEventsListView {
    final class ViewModel: ObservableObject {
        private var subscriptions = Set<AnyCancellable>()
        
        var calendarEvents: [CalendarEvent]


        // MARK: - Init
        init(
            calendarEvents: [CalendarEvent]
        ) {
            self.calendarEvents = calendarEvents
        }
    }
}


// MARK: - Computeds
extension CalendarEventsListView.ViewModel {
    var cardCount: Int { calendarEvents.count }
}
