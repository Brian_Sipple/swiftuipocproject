//
//  PreviewData+CalendarEvents.swift
//  TimelessSampleProject
//
//  Created by CypherPoet on 3/24/20.
// ✌️
//

import Foundation


extension PreviewData {
    
    enum CalendarEvents {
        
        private static let descriptionText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        
        
        static let sample1: CalendarEvent = {
            .init(
                title: "Discuss Many Things",
                longDescription: descriptionText,
                startDate: startDate1,
                endDate: endDate1,
                cardImageName: "card-1",
                repeatsWeekly: true,
                repeatsWeeklyDescription: "Repeats weekly on Mon, Wed, Sat",
                dateOfLastRepeat: Date(),
                location: PreviewData.EventLocations.blueBottle
            )
        }()

        
        static let sample2: CalendarEvent = {
            .init(
                title: "Investor Meeting at Greylock",
                longDescription: descriptionText,
                startDate: startDate2,
                endDate: endDate2,
                cardImageName: "card-2",
                repeatsWeekly: true,
                repeatsWeeklyDescription: "Repeats weekly on Mon, Wed, Sat",
                dateOfLastRepeat: Date(),
                location: PreviewData.EventLocations.philz
            )
        }()
        
        
        static let sample3: CalendarEvent = {
            .init(
                title: "Moar Coffee",
                longDescription: descriptionText,
                startDate: startDate3,
                endDate: endDate3,
                cardImageName: "card-1",
                repeatsWeekly: false,
                location: PreviewData.EventLocations.blueBottle
            )
        }()
        
        
        static let all: [CalendarEvent] = [Self.sample1, Self.sample2, Self.sample3]
    }
}


// MARK: - Dates
extension PreviewData.CalendarEvents {
    
    private static let startDate1 = DateComponents(
        calendar: .current,
        year: 2020,
        month: 4,
        day: 1,
        hour: 6,
        minute: 30
    ).date!

    private static let endDate1 = DateComponents(
        calendar: .current,
        year: 2020,
        month: 4,
        day: 1,
        hour: 7,
        minute: 30
    ).date!
    
    
    
    private static let startDate2 = DateComponents(
        calendar: .current,
        year: 2020,
        month: 4,
        day: 1,
        hour: 7,
        minute: 30
    ).date!

    private static let endDate2 = DateComponents(
        calendar: .current,
        year: 2020,
        month: 4,
        day: 1,
        hour: 9,
        minute: 30
    ).date!
    
    
    
    private static let startDate3 = DateComponents(
        calendar: .current,
        year: 2020,
        month: 4,
        day: 1,
        hour: 12,
        minute: 30
    ).date!

    private static let endDate3 = DateComponents(
        calendar: .current,
        year: 2020,
        month: 4,
        day: 1,
        hour: 14,
        minute: 30
    ).date!
}
