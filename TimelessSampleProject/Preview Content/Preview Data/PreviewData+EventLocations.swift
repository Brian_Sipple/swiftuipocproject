//
//  PreviewData+EventLocations.swift
//  TimelessSampleProject
//
//  Created by CypherPoet on 3/25/20.
// ✌️
//

import Foundation
import CoreLocation



extension PreviewData {
    
    enum EventLocations {
        
        static let blueBottle: EventLocation = {
            .init(
                latitude: 37.447719,
                longitude: -122.1596787,
                title: "Blue Bottle Coffee",
                addressLine: "456 University Ave, Palo Alto, CA 94301"
            )
        }()
        
        
        static let philz: EventLocation = {
            .init(
                latitude: 37.443443,
                longitude: -122.1622482,
                title: "Philz Coffee",
                addressLine: "101 Forest Ave, Palo Alto, CA 94306"
            )
        }()
    }
}
