//
//  PreviewData+AppStores.swift
//  TimelessSampleProject
//
//  Created by CypherPoet on 3/25/20.
// ✌️
//

import Foundation


extension PreviewData {
    
    enum AppStores {
        
        static let initial: AppStore = {
            AppStore(
                initialState: AppState(),
                appReducer: appReducer
            )
        }()
    }
}
