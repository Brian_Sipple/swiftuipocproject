//
//  CalendarEventActionModal.swift
//  TimelessSampleProject
//
//  Created by CypherPoet on 3/27/20.
// ✌️
//

import SwiftUI


struct CalendarEventActionModal {
    var event: CalendarEvent
}


// MARK: - View
extension CalendarEventActionModal: View {

    var body: some View {
        RoundedRectangle(cornerRadius: 22)
            .fill(Color(UIColor.secondarySystemBackground))
            .overlay(modalContent)
    }
}


// MARK: - Computeds
extension CalendarEventActionModal {
}


// MARK: - View Variables
extension CalendarEventActionModal {
    
    private var modalContent: some View {
        VStack {
            Text("Action Menu for \"\(event.title)\"")
                .font(.headline)
            
            Spacer()
        }
        .padding()
    }
}


// MARK: - Private Helpers
private extension CalendarEventActionModal {
}



// MARK: - Preview
struct CalendarEventActionModal_Previews: PreviewProvider {

    static var previews: some View {
        CalendarEventActionModal(event: PreviewData.CalendarEvents.sample1)
    }
}
