//
//  EventIntervalTrack.swift
//  TimelessSampleProject
//
//  Created by CypherPoet on 3/27/20.
// ✌️
//

import SwiftUI


struct EventIntervalTrack {
    var startDate: Date
    var endDate: Date
    
    var color: Color = .white
}


// MARK: - View
extension EventIntervalTrack: View {

    var body: some View {
        HStack {
            linePiece
            startSymbol
            endSymbol
            linePiece
        }
        .font(.system(size: 13))
    }
}


// MARK: - View Variables
extension EventIntervalTrack {
    
    private var linePiece: some View {
        RoundedRectangle(cornerRadius: .infinity)
            .fill(color)
            .frame(height: 2)
    }
}


// MARK: - Computeds
extension EventIntervalTrack {
    
    var startSymbol: some View {
        Image(systemName: startDate.isInDaylight ? "sun.min.fill" : "moon.fill")
    }
    
    var endSymbol: some View {
        Image(systemName: endDate.isInDaylight ? "sun.min.fill" : "moon.fill")
    }
}


// MARK: - Preview
struct IntervalTrackLine_Previews: PreviewProvider {

    static var previews: some View {
        EventIntervalTrack(
            startDate: Date(),
            endDate: Date().addingTimeInterval(60 * 60 * 12)
        )
    }
}
