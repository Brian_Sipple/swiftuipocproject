//
//  CalendarEventsListCard+ViewModel.swift
//  TimelessSampleProject
//
//  Created by Brian Sipple on 3/27/20.
//  Copyright © 2020 CypherPoet. All rights reserved.
//

import Foundation


extension CalendarEventsListCard {
    
    struct ViewModel {
        var event: CalendarEvent
        var displayMode: DisplayMode = .listFace
    }
}



// MARK: - Computeds
extension CalendarEventsListCard.ViewModel {
    
    var titleText: String { event.title }
    
    var isShowingLocation: Bool { event.location != nil && displayMode == .listFace }
    var locationTitleText: String { event.location?.title ?? "" }
    var locationAddressText: String { event.location?.addressLine ?? "" }
    
    
    var eventStartTimeText: String { Formatters.eventCardTime.string(from: event.startDate).lowercased() }
    var eventStartDateText: String { Formatters.eventCardDate.string(from: event.startDate) }
    
    var eventEndTimeText: String { Formatters.eventCardTime.string(from: event.endDate) }
    var eventEndDateText: String { Formatters.eventCardDate.string(from: event.endDate) }
    
    var eventStartOrEndProximityText: String {
        guard event.isUpcoming else { return "" }
        
        return "in \(event.minutesToStart) min"
    }
}


// MARK: - Display Mode
extension CalendarEventsListCard.ViewModel {
    enum DisplayMode {
        case listFace
        case expansion
    }
}
