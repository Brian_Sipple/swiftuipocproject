//
//  CalendarEventsListCard.swift
//  TimelessSampleProject
//
//  Created by CypherPoet on 3/25/20.
// ✌️
//

import SwiftUI
import CypherPoetSwiftUIKit


struct CalendarEventsListCard {
    var viewModel: ViewModel
}


// MARK: - View
extension CalendarEventsListCard: View {

    var body: some View {
        GeometryReader { geometry in
            ZStack {
                Image(self.viewModel.event.cardImageName)
                    .renderingMode(.original)
                    .resizable()
                    .scaledToFill()
                    .frame(
                        width: geometry.size.width,
                        height: geometry.size.height
                    )
                
                self.foregroundContent
                    .position(x: geometry.frame(in: .local).midX, y: geometry.frame(in: .local).midY)
            }
            .clipped()
            .cornerRadius(geometry.size.height * 0.03)
            .foregroundColor(.white)
        }
    }
}


// MARK: - View Variables
extension CalendarEventsListCard {
    
    private var foregroundContent: some View {
        VStack {
            if viewModel.displayMode == .expansion {
                headerSection
                    .offset(x: 0, y: -Constants.Appearance.homeHeaderHeight * 0.5)
                    .transition(contentSectionTransition)
            }
            
            titleSection
            
            Spacer()
            
            footerSection
        }
        .padding(.horizontal, 20)
        .padding(.vertical, extraVerticalPadding)
    }
    
    
    private var headerSection: some View {
        HStack {
            Image(systemName: "square.and.pencil")
                .imageScale(.large)
           
            Spacer()
            
            Text(viewModel.eventStartOrEndProximityText)
                .fontWeight(.medium)
                .padding(.horizontal, 14)
                .padding(.vertical, 6)
                .background(
                    Capsule()
                        .stroke(Color.white, lineWidth: 1)
                )
        }
    }
    
    
    private var titleSection: some View {
        HStack {
            GeometryReader { geometry in
                Text(self.viewModel.titleText)
                    .font(.largeTitle)
                    .fontWeight(.bold)
                    .padding(.trailing, geometry.size.width * 0.2)  // Wrap to second line before hitting the full width
                    .lineLimit(2)
                    .fixedSize(horizontal: false, vertical: true)
                    .multilineTextAlignment(.leading)
                
                Spacer()
            }
            .padding(.vertical, viewModel.displayMode == .expansion ? 0 : 16)
        }
    }
    
    
    private var footerSection: some View {
        VStack(spacing: 16) {
            if viewModel.isShowingLocation {
                locationDescriptionSection
                    .transition(contentSectionTransition)
            }
            
            dateIntervalSection
        }
    }
    
    
    private var locationDescriptionSection: some View {
        HStack {
            VStack(alignment: .leading, spacing: 4) {
                Text(viewModel.locationTitleText)
                    .scaledSystemFont(size: 18)
                Text(viewModel.locationAddressText)
                    .font(.caption)
            }
            .lineLimit(1)
            
            Spacer()
        }
    }
    
    
    private var dateIntervalSection: some View {
        HStack(spacing: 12) {
            VStack(alignment: .leading, spacing: 2) {
                Text(viewModel.eventStartTimeText)
                    .scaledSystemFont(size: 22)
                Text(viewModel.eventStartDateText)
            }
            
            EventIntervalTrack(
                startDate: viewModel.event.startDate,
                endDate: viewModel.event.endDate
            )
            
            VStack(alignment: .trailing, spacing: 2) {
                Text(viewModel.eventEndTimeText)
                    .scaledSystemFont(size: 22)
                Text(viewModel.eventEndDateText)
            }
        }
    }
}


// MARK: - Animations & Transitions
extension CalendarEventsListCard {
    
    var contentSectionTransition: AnyTransition {
        AnyTransition
            .opacity
            .animation(.easeInOut(duration: Constants.Animation.Duration.cardSlide))
    }
}

// MARK: - Computeds
extension CalendarEventsListCard {
    
    var extraVerticalPadding: CGFloat {
        if viewModel.displayMode == .expansion {
            return Constants.Appearance.homeHeaderHeight * 1.5
        } else {
            return 20
        }
    }
}


// MARK: - Preview
struct CalendarEventsListCard_Previews: PreviewProvider {

    static var previews: some View {
        CalendarEventsListCard(
            viewModel: .init(
                event: PreviewData.CalendarEvents.sample1,
                displayMode: .listFace
            )
        )
        .frame(width: 343, height: 478.96)
    }
}
