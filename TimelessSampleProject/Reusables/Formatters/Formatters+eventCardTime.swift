//
//  Formatters+eventCardTime.swift
//  TimelessSampleProject
//
//  Created by CypherPoet on 3/27/20.
// ✌️
//

import Foundation


extension Formatters {
    
    static let eventCardTime: DateFormatter = {
        let formatter = DateFormatter()
        
        formatter.dateStyle = .none
        formatter.timeStyle = .short
        
        return formatter
    }()
}
