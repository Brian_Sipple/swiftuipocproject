//
//  Formatters+eventCardDate.swift
//  TimelessSampleProject
//
//  Created by CypherPoet on 3/27/20.
// ✌️
//

import Foundation



extension Formatters {
    
    static let eventCardDate: DateFormatter = {
        let formatter = DateFormatter()
        
        formatter.dateFormat = "E, MMM d"
        
        return formatter
    }()
}
