//
//  Formatters+weekday.swift
//  TimelessSampleProject
//
//  Created by CypherPoet on 3/27/20.
// ✌️
//

import Foundation


extension Formatters {
    
    /// Displays weekday text for a date (e.g: "Monday", "Tuesday")
    static let weekday: DateFormatter = {
        let formatter = DateFormatter()
        
        formatter.dateFormat = "EEEE"
        
        return formatter
    }()
}
