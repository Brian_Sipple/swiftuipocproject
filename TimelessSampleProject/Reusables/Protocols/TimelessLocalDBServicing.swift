//
//  TimelessLocalDBServicing.swift
//  TimelessSampleProject
//
//  Created by CypherPoet on 3/25/20.
// ✌️
//

import Foundation
import Combine


protocol TimelessLocalDBServicing {
    
    // 📝 This is how I would envision structuring a service that talked to
    // the backend DB -- even though we'll just be returning sample data for now.
    var session: URLSession { get }
    var apiQueue: DispatchQueue { get }
    
    
    func fetchUpcomingEvents(
        using decoder: JSONDecoder
    ) -> AnyPublisher<[CalendarEvent], LocalDBServiceError>
}


// MARK: - Default Implementation
extension TimelessLocalDBServicing {
    
    func fetchUpcomingEvents(
        using decoder: JSONDecoder = .init()
    ) -> AnyPublisher<[CalendarEvent], LocalDBServiceError> {
        // 📝 This is the point where'd I'd envision implementing more "real" networking
        // logic in an fuller app that was accessing a production backend.
        Just(PreviewData.CalendarEvents.all)
            .setFailureType(to: LocalDBServiceError.self)
            .eraseToAnyPublisher()
    }
}
