//
//  Constants+Appearance.swift
//  TimelessSampleProject
//
//  Created by CypherPoet on 3/27/20.
// ✌️
//

import SwiftUI


extension Constants {
    
    enum Appearance {
        static let minCardHeight: CGFloat = 300.0
        static let homeHeaderHeight: CGFloat = 84
    }
}
