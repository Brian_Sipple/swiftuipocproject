//
//  Constants+Animation.swift
//  TimelessSampleProject
//
//  Created by CypherPoet on 3/27/20.
// ✌️
//

import SwiftUI


extension Constants {
    
    enum Animation {

        enum Duration {
            static let cardSlide = 0.34
            static let modalAppearance = 0.3
        }
    }
}
