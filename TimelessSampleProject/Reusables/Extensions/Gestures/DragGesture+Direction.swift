//
//  DragGesture+Utils.swift
//  TimelessSampleProject
//
//  Created by CypherPoet on 4/2/20.
// ✌️
//

import SwiftUI


extension DragGesture.Value {
    
    enum Direction {
        case left
        case right
        case up
        case down
        case none
    }
    
    
    var dominantDirection: Direction {
        guard predictedEndTranslation != .zero else { return .none }
        
        let (width, height) = (predictedEndTranslation.width, predictedEndTranslation.height)
        
        if width > height {
            return width < 0 ? .left : .right
        } else {
            return height < 0 ? .up : .down
        }
    }
}
