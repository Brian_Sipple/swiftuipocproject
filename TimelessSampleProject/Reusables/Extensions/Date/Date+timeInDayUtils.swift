//
//  Date+timeInDayUtils.swift
//  TimelessSampleProject
//
//  Created by CypherPoet on 3/27/20.
// ✌️
//

import Foundation


extension Date {
    
    var currentHour: Int {
        Calendar.current.dateComponents(
            [.hour],
            from: self
        )
        .hour ?? 0
    }
    
    
    var isInDaylight: Bool {
        (7...18).contains(currentHour)
    }
}
