//
//  Date+displayUtils.swift
//  TimelessSampleProject
//
//  Created by CypherPoet on 3/27/20.
// ✌️
//

import Foundation


extension Date {
    
    static var todaysDayText: String {
        Formatters.weekday.string(from: Date())
    }
}
