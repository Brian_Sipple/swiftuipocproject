//
//  LocalDBServiceError.swift
//  TimelessSampleProject
//
//  Created by CypherPoet on 3/25/20.
// ✌️
//

import Foundation


enum LocalDBServiceError: Error {
    // 📝 Ideally, this would be where we could wrap any `Error` type returned by the backend for our client application's unique requirements.
    case network(error: Error)
}



extension LocalDBServiceError {
    
    public var errorDescription: String? {
        switch self {
        case .network(let error):
            return "An error occurred while attempting to access the Timeless local database: \(error.localizedDescription)"
        }
    }
}


// MARK: -  Identifiable
extension LocalDBServiceError: Identifiable {
    public var id: String? { errorDescription }
}
