//
//  LocalDBService.swift
//  TimelessSampleProject
//
//  Created by CypherPoet on 3/25/20.
// ✌️
//

import Foundation


public final class LocalDBService {
    public var session: URLSession
    public var apiQueue: DispatchQueue
    
    public init(
        session: URLSession = .shared,
        queue: DispatchQueue = DispatchQueue(label: "TranslationAPIService", qos: .userInitiated)
    ) {
        self.session = session
        self.apiQueue = queue
    }
}


extension LocalDBService: TimelessLocalDBServicing {}
