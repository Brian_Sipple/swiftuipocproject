//
//  AppStore.swift
//  TimelessSampleProject
//
//  Created by CypherPoet on 3/25/20.
// ✌️
//

import Foundation
import CypherPoetSwiftUIKit_DataFlowUtils


typealias AppStore = Store<AppState, AppAction>
